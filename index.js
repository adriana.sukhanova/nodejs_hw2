require('dotenv').config();

const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

// mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true });
mongoose.connect('mongodb+srv://adrianas:qwert12345@cluster0.lhr0l9m.mongodb.net/?retryWrites=true&w=majority', { useNewUrlParser: true });

const userAuthRouter = require('./src/userAuthRouter');
const userRouter = require('./src/userRouter');
const notesRouter = require('./src/notesRouter');

app.use(express.json());
app.use(express.urlencoded());
app.use(morgan('tiny'));

app.use('/api/auth', userAuthRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

// app.listen(process.env.PORT);
app.listen(8080);

app.use(errorHandler);

function errorHandler(err, req, res) {
  if (err.status === undefined) {
    res.status(400).send({ message: err.message });
    return;
  }
  if (err) {
    res.status(err.status).send({ message: err.message });
    return;
  }
  res.status(500).send({ message: 'Server error' });
}
