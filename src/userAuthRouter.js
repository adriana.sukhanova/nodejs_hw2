const express = require('express');

const router = express.Router();
const { createProfile, login } = require('./userService');

router.post('/register', createProfile);
router.post('/login', login);

module.exports = router;
