const express = require('express');

const router = express.Router();
const {
  getProfileInfo, changeProfilePassword, deleteProfile,
} = require('./userService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getProfileInfo);
router.patch('/me', authMiddleware, changeProfilePassword);
router.delete('/me', authMiddleware, deleteProfile);

module.exports = router;
