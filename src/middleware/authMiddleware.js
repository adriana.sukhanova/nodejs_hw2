require('dotenv').config();

const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const auth = req.headers.authorization;

  if (!auth) {
    return res.status(400).send({ message: 'Please provide authorisation header' });
  }
  const token = auth.split(' ')[1];

  if (!token) {
    return res.status(400).send({ message: 'Please include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret_key'); // process.env.SECRET_JWT_KEY,
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    return res.status(401).send({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
