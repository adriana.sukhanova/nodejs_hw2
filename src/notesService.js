const date = require('date-and-time');
const { Note } = require('./models/Note');

const getUserNotes = (req, res) => Note
  .find({ userId: req.user.userId }, '-__v')
  .then((userNotes) => res.status(200).json({
    offset: req.body.offset || 0,
    limit: req.body.limit || 0,
    count: 0,
    notes: userNotes,
  }));

const addUserNotes = (req, res) => {
  const { text } = req.body;
  const { userId } = req.user;

  const note = new Note({
    userId,
    completed: false,
    text,
    createdDate: date.format(new Date(), 'YYYY-MM-DDTHH:mm:ssZ'),
  });

  note.save().then(() => res.status(200).json({ message: 'Note was successfully saved' }));
};

const getUserNoteById = (req, res) => Note
  .findById({
    _id: req.params.id,
    userId: req.user.userId,
  })
  .then((result) => res.status(200).json({ note: result }));

const updateUserNoteById = (req, res) => Note
  .findByIdAndUpdate(
    {
      _id: req.params.id,
      userId: req.user.userId,
    },
    { $set: { text: req.body.text } },
  )
  .then(() => res.status(200).json({ message: 'Note was successfully updated' }));

const toggleCompletedForUserNoteById = (req, res) => Note
  .findByIdAndUpdate(
    {
      _id: req.params.id,
      userId: req.user.userId,
    },
    { $set: { completed: true } },
  )
  .then(() => res.status(200).json({ message: 'Note was successfully checked/unchecked' }));

const deleteUserNoteById = (req, res) => Note
  .findByIdAndDelete({
    _id: req.params.id,
    userId: req.user.userId,
  })
  .then(() => res.status(200).json({ message: 'Note was succesfully deleted' }));

module.exports = {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
};
