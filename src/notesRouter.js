const express = require('express');

const router = express.Router();
const {
  getUserNotes, addUserNotes, getUserNoteById, updateUserNoteById,
  toggleCompletedForUserNoteById, deleteUserNoteById,
} = require('./notesService');

const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/', authMiddleware, getUserNotes);
router.post('/', authMiddleware, addUserNotes);

router.get('/:id', authMiddleware, getUserNoteById);
router.put('/:id', authMiddleware, updateUserNoteById);
router.patch('/:id', authMiddleware, toggleCompletedForUserNoteById);
router.delete('/:id', authMiddleware, deleteUserNoteById);

module.exports = router;
