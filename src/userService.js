const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const date = require('date-and-time');
const { User } = require('./models/User');

const createProfile = async (req, res) => {
  const { username, password } = req.body;

  if (username === '' || password === '') {
    return res.status(400).json({ message: 'Please enter all data' });
  }

  const newUser = new User({
    username,
    password: await bcrypt.hash(password, 10),
    createdDate: date.format(new Date(), 'YYYY-MM-DDTHH:mm:ssZ'),
  });

  await newUser.save()
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch((err) => res.status(400).send({ message: err.message }));
};

const login = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (
    !user
    && !bcrypt.compare(String(req.body.password), String(user.password))
  ) {
    return res.status(403).json({ message: 'Not authorized' });
  }
  const token = jwt.sign({
    username: user.username,
    userId: user._id,
  }, 'secret_key'); // process.env.SECRET_JWT_KEY,
  return res.status(200).json({ message: 'Success', jwt_token: token });
};

const getProfileInfo = async (req, res) => {
  await User
    .findById(req.user.userId)
    .then((result) => res.status(200).json({
      user: {
        _id: result._id,
        username: result.username,
        createdDate: result.createdDate,
      },
    }));
};

const changeProfilePassword = (req, res) => User.findByIdAndUpdate(
  { _id: req.user.userId, password: req.body.oldPassword },
  { $set: { password: req.body.newPassword } },
  (err) => {
    if (err) res.status(400).json({ message: 'Something went wrong' });
    return res.status(200).json({ message: 'Password was successfully changed' });
  },
);

const deleteProfile = (req, res) => User.findByIdAndDelete({ _id: req.user.userId })
  .then(() => res
    .status(200)
    .json({ message: `User ${req.user.username} was successfully deleted` }))
  .catch((err) => res.status(err.status).json({ message: err.message }));

module.exports = {
  createProfile,
  login,
  getProfileInfo,
  changeProfilePassword,
  deleteProfile,
};
