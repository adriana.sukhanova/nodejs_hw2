const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  completed: {
    type: Boolean,
    required: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: false,
  },
});

module.exports = {
  Note,
};
